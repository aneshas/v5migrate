package exporter

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"bitbucket.org/claireit/v5migrate"
	"cloud.google.com/go/datastore"
	"golang.org/x/net/context"
	dstore "google.golang.org/appengine/datastore"
)

var appQuery = `
Select
checklistID,
cat.category_name,
cat.Disp_order,
cat.location_id,
chk.checklist_name,
que.sub_category_name,
que.Sub_Disp_Order,
anw.description,
anw.status,
anw.completed,
anw.customer_approved,
anw.price,
img.imagedisplay
From tbl_appointment_checklist_status
JOIN tbl_category cat ON cat.company_id = %d 
JOIN tbl_checklist_category rel ON cat.category_id = rel.category_id
JOIN tbl_sub_category que ON cat.category_id = que.category_id
JOIN tbl_query_status anw ON anw.question_id = que.sub_category_id AND anw.tbl_app_id = %d 
JOIN tbl_checklist chk ON chk.checklist_id = rel.checklist_id
LEFT JOIN tbl_car_image img ON img.tbl_app_id = %d AND img.company_id = %d AND img.question_id = anw.question_id
/*first join selects category names belonging to provided company id*/
/*second join selects the ids for the categories belonging to the checklist id*/
/*third join selects the question names based on the ids from the second join*/
where
checklistID = rel.checklist_id
AND appointmentID = %d 
`

var questionStatus = map[string]int{
	"ok":      1,
	"null":    1,
	"checked": 2,
	"alert":   3,
}

var images []string

type Exporter interface {
	// company_id
	GetExportIDs(string) ([]int64, error)
	Export(context.Context, []int64) ([]v5migrate.ExportEntry, error)
}

type sqlExporter struct {
	conn   *sql.DB
	client *datastore.Client
	info   v5migrate.DealerInfo
}

func (e *sqlExporter) GetExportIDs(until string) ([]int64, error) {
	locFilter := "("
	for id, _ := range e.info.LocationInfo {
		locFilter += fmt.Sprintf("%d, ", id)
	}

	locFilter = strings.TrimRight(locFilter, ", ")
	locFilter += ")"

	query := fmt.Sprintf("select app_id from tbl_appointment where car_status IN (2, 3, 4, 5, 7) AND company_id=%d AND location_id in %s and date_of_appointment <= '%s'", e.info.DealerIDv5, locFilter, until)

	rows, err := e.conn.Query(query)
	if err != nil {
		return nil, err
	}

	var ids []int64

	for rows.Next() {
		var id int64
		err := rows.Scan(&id)
		if err != nil {
			log.Printf("Error scanning app_id: %v", err)
			continue
		}

		ids = append(ids, id)
	}

	return ids, nil
}

func (e *sqlExporter) Export(c context.Context, ids []int64) ([]v5migrate.ExportEntry, error) {
	appFilter := "("
	for _, id := range ids {
		appFilter += fmt.Sprintf("%d, ", id)
	}

	appFilter = strings.TrimRight(appFilter, ", ")
	appFilter += ")"

	var statuses []v5migrate.AppointmentStatus
	var result []v5migrate.ExportEntry
	var count int

	q := datastore.NewQuery("AppointmentStatus")
	keys, err := e.client.GetAll(c, q, &statuses)
	if err != nil {
		log.Fatal(err)
	}

	statusesV6 := make(map[int]*datastore.Key)

	for i, status := range statuses {
		statusesV6[status.Identifier] = keys[i]
	}

	log.Println("Reading appointments")
	query := fmt.Sprintf("select app.*, loc.location_name from tbl_appointment app JOIN tbl_location loc ON loc.location_id = app.location_id where app_id IN %s", appFilter)

	rows, err := e.conn.Query(query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		entry := v5migrate.ExportEntry{}

		legID, err := getAppData(rows, statusesV6, &entry)
		if err != nil {
			if err == ErrMissingLocationID {
				log.Println(err)
				continue
			}

			return nil, err
		}

		locID := e.info.LocationInfo[legID]
		locKeyGAE, err := dstore.DecodeKey(locID)
		if err != nil {
			log.Println(err)
			return nil, ErrDecodingLocKey
		}

		parentKey := datastore.IDKey("Dealer", locKeyGAE.Parent().IntID(), nil)
		locKey := datastore.IDKey("DealerLocation", locKeyGAE.IntID(), parentKey)

		dealKeyGAE, err := dstore.DecodeKey(e.info.DealerIDv6)
		if err != nil {
			log.Println(err)
			log.Println(e.info.DealerIDv6)
			return nil, ErrDecodingDealKey
		}

		dealKey := datastore.IDKey("Dealer", dealKeyGAE.IntID(), nil)

		entry.Appointment.DealerLocationID = locKey
		entry.Car.DealerLocationID = locKey

		_ = dealKey

		result = append(result, entry)

		count += 1
	}

	rows.Close()

	log.Printf("Read %d appointments from the database", count)
	log.Println("Reading checks")

	for i, entry := range result {
		query := fmt.Sprintf(appQuery,
			e.info.DealerIDv5,
			entry.Appointment.IDV5,
			entry.Appointment.IDV5,
			e.info.DealerIDv5,
			entry.Appointment.IDV5)

		rows, err := e.conn.Query(query)
		if err != nil {
			log.Printf("Could not fetch checks for app_id %d - %v", entry.Appointment.IDV5, err)
			continue
		}

		var qItemsV5 []v5migrate.QuestionItemV5

		for rows.Next() {
			qItemV5 := v5migrate.QuestionItemV5{}

			err := rows.Scan(&qItemV5.ChecklistID,
				&qItemV5.CategoryName,
				&qItemV5.DisplayOrder,
				&qItemV5.LocationID,
				&qItemV5.ChecklistName,
				&qItemV5.QuestionTitle,
				&qItemV5.SubDisplayOrder,
				&qItemV5.Description,
				&qItemV5.Status,
				&qItemV5.Completed,
				&qItemV5.CustomerApproved,
				&qItemV5.Price,
				&qItemV5.ImageName)

			if err != nil {
				log.Printf("Could not scan qItem for app_id %d - %v", entry.Appointment.LocationIDV5, err)
				continue
			}

			qItemsV5 = append(qItemsV5, qItemV5)
		}

		rows.Close()

		result[i].Checks = getV6Checks(qItemsV5)
	}

	imageBytes, _ := json.Marshal(images)
	fname := fmt.Sprintf("%d_images%d.json", e.info.DealerIDv5, ids[0])
	ioutil.WriteFile(fname, []byte(imageBytes), os.ModePerm)

	return result, nil
}

func getV6Checks(qItemsV5 []v5migrate.QuestionItemV5) []v5migrate.CarCheck {
	checksV5 := make(map[int64][]*v5migrate.QuestionItemV5)

	for i, qItem := range qItemsV5 {
		checksV5[qItem.ChecklistID] = append(checksV5[qItem.ChecklistID], &qItemsV5[i])
	}

	var checks []v5migrate.CarCheck

	for _, qItems := range checksV5 {
		check := v5migrate.CarCheck{
			CreatedOn: time.Now(),
			Checklist: v5migrate.Checklist{
				Name: qItems[0].ChecklistName,
			},
		}

		for _, qItem := range qItems {
			item := v5migrate.QuestionItem{
				GroupName:  qItem.CategoryName,
				GroupOrder: int(qItem.DisplayOrder),
				Title:      qItem.QuestionTitle,
				Raw:        qItem.Description.String,
			}

			if qItem.Completed == "y" {
				item.Completed = true
			}

			if qItem.CustomerApproved == "y" {
				item.CustomerApproved = true
			}

			if qItem.Description.String == "" && (qItem.Status == "ok" || qItem.Status == "null") {
				continue
			}

			st, ok := questionStatus[qItem.Status]
			if ok {
				item.Status = st
			}

			if qItem.ImageName.String != "" {
				images = append(images, qItem.ImageName.String)
				image := v5migrate.Image{
					// TODO - Make image url point to cloud storage
					URL:          "https://storage.googleapis.com/v5images/claireit.com/car_images/" + qItem.ImageName.String,
					Active:       true,
					VisibleInPDF: true,
				}

				item.Images = []v5migrate.Image{image}
			}

			check.QuestionItems = append(check.QuestionItems, item)
		}

		checks = append(checks, check)
	}

	return checks
}

var ErrMissingLocationID = errors.New("Appointment missing location id.")
var ErrDecodingLocKey = errors.New("Could not decode location key")
var ErrDecodingDealKey = errors.New("Could not decode dealer key")

func getAppData(rows *sql.Rows, statuses map[int]*datastore.Key, entry *v5migrate.ExportEntry) (int64, error) {
	var appv5 v5migrate.AppointmentV5

	err := rows.Scan(&appv5.Id,
		&appv5.CompanyID,
		&appv5.Number,
		&appv5.Dmsnr,
		&appv5.Date,
		&appv5.Manufacture,
		&appv5.CarModel,
		&appv5.RegistrationNumber,
		&appv5.Title,
		&appv5.Initial,
		&appv5.Name,
		&appv5.Email,
		&appv5.Postcode,
		&appv5.ContactNumber1,
		&appv5.ContactNumber2,
		&appv5.VinNumber,
		&appv5.WorkNumber,
		&appv5.Description,
		&appv5.StatusId,
		&appv5.UserId,
		&appv5.InterventionText,
		&appv5.APKDate,
		&appv5.DBWorkinNow,
		&appv5.LocationId,
		&appv5.NextKm,
		&appv5.NextDate,
		&appv5.ChecklistId,
		&appv5.AaNnameChecklistId,
		&appv5.CurrentKm,
		&appv5.AppointmentIn,
		&appv5.AppointmentOut,
		&appv5.Waiting,
		&appv5.CreatedOn,
		&appv5.UpdatedOn,
		&appv5.LocationName)

	if err != nil {
		return 0, err
	}

	if appv5.LocationId.IsZero() {
		return 0, ErrMissingLocationID
	}

	entry.Appointment = v5migrate.Appointment{
		CarMake:   appv5.Manufacture,
		CarModel:  appv5.CarModel,
		RegNumber: appv5.RegistrationNumber,
		DMSNr:     appv5.Number,
		WONr:      appv5.WorkNumber,
		// CreatedOn:    time.Now(),
		// TimeCarApp:   time.Now(),
		IsLegacy:            true,
		LocationIDV5:        appv5.LocationId.Int64,
		IDV5:                appv5.Id,
		LocationNameV5:      appv5.LocationName.String,
		AppointmentStatusV5: appv5.StatusId,
	}

	appDate, err := GetTime(appv5.Date)
	if err == nil {
		entry.Appointment.CreatedOn = appDate
		entry.Appointment.TimeCarApp = appDate
	}

	if appv5.InterventionText.String != "" {
		entry.Appointment.Intervention = []v5migrate.Intervention{
			{
				Title: appv5.InterventionText.String,
			},
		}
	}

	statusID, ok := statuses[int(appv5.StatusId)]
	if !ok {
		log.Printf("No corresponding V6 status for identifier %d", appv5.StatusId)
		statusID = nil
	}

	entry.Appointment.AppointmentStatusID = statusID

	entry.Car = v5migrate.Car{
		VINNr:     appv5.VinNumber,
		Make:      appv5.Manufacture,
		Model:     appv5.CarModel,
		RegNr:     appv5.RegistrationNumber,
		CreatedOn: time.Now(),
	}

	apkDate, err := GetTime(appv5.APKDate)
	if err == nil {
		entry.Car.APKDateDMS = apkDate
	}

	if !appv5.CurrentKm.IsZero() {
		km, err := strconv.Atoi(appv5.CurrentKm.String)
		if err == nil {
			entry.Car.CurrentKM = km
			entry.Appointment.CurrentKM = km
		}
	}

	if !appv5.NextKm.IsZero() {
		km, err := strconv.Atoi(appv5.NextKm.String)
		if err == nil {
			entry.Car.NextKM = km
			entry.Appointment.NextKM = km
		}
	}

	if !appv5.NextDate.IsZero() {
		nd, err := GetTime(appv5.NextDate.String)
		if err == nil {
			entry.Appointment.NextDate = nd
		}
	}

	// TODO - Trim all strings

	return appv5.LocationId.Int64, nil
}

func NewMySQLExporter(connStr string, client *datastore.Client, info v5migrate.DealerInfo) (Exporter, func(), error) {
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		return nil, func() {}, err
	}

	var close = func() { db.Close() }

	return &sqlExporter{db, client, info}, close, nil
}

func GetTime(str string) (time.Time, error) {
	layout := "2006-01-02T15:04:05.000Z"
	str += "T13:00:00.000Z"

	t, err := time.Parse(layout, str)
	if err != nil {
		log.Println(err, str)
		return time.Now(), err
	}

	zone, _ := time.LoadLocation("Europe/Amsterdam")
	return t.In(zone), nil
}
