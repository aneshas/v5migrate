package v5migrate

import "gopkg.in/guregu/null.v2"

// Appointment entity
type AppointmentV5 struct {
	Id                 int64       `json:"id" sql:"app_id" gorm:"column:app_id;primary_key"`
	Number             string      `json:"number" sql:"appointment_no" gorm:"column:appointment_no"`
	Dmsnr              int64       `json:"dmsnr" sql:"dmsnr"`
	Date               string      `json:"date" sql:"date_of_appointment" gorm:"column:date_of_appointment"`
	CompanyID          string      `sql:"company_id"`
	Manufacture        string      `json:"manufacture" sql:"manufacture"`
	CarModel           string      `json:"car_model" sql:"car_model"`
	RegistrationNumber string      `json:"registration_number" sql:"reg_no" gorm:"column:reg_no"`
	Title              string      `json:"title" sql:"title"`
	Initial            string      `json:"initial" sql:"initial"`
	Name               string      `json:"name" sql:"name"`
	Email              string      `json:"email" sql:"email"`
	Postcode           string      `json:"postcode" sql:"postcode"`
	ContactNumber1     string      `json:"contact_number_1" sql:"contact_no1" gorm:"column:contact_no1"`
	ContactNumber2     string      `json:"contact_number_2" sql:"contact_no2" gorm:"column:contact_no2"`
	VinNumber          string      `json:"vin_number" sql:"vin_no" gorm:"column:column:vin_no"`
	WorkNumber         string      `json:"work_number" sql:"work_no" gorm:"column:work_no"`
	Description        string      `json:"description" sql:"description"`
	InterventionText   null.String `json:"intervention,omitempty" sql:"Intervation"`
	APKDate            string      `json:"apk_date" sql:"APKdate" gorm:"column:APKdate"`
	NextKm             null.String `json:"next_km,omitempty" sql:"next_km"`
	NextDate           null.String `json:"next_date,omitempty" sql:"next_date"`
	CurrentKm          null.String `json:"current_km,omitempty" sql:"current_km"`
	AppointmentIn      string      `json:"appointment_in" sql:"appIn" gorm:"column:appIn"`
	AppointmentOut     string      `json:"appointment_out" sql:"appOut" gorm:"column:appOut"`
	Waiting            string      `json:"waiting" sql:"waiting"`
	DBWorkinNow        string      `sql:"db_workin_now"`
	CreatedOn          string      `json:"created_on" sql:"createdOn" gorm:"column:createdOn"`
	UpdatedOn          string      `json:"updated_on" sql:"updatedOn" gorm:"column:updatedOn"`
	AaNnameChecklistId int64       `json:"aanname_checklist_id" sql:"aanname_ckecklist_id" gorm:"column:aanname_checklist_id"`
	UserId             int64       `json:"user_id" sql:"user_id"`
	LocationId         null.Int    `json:"location_id,omitempty" sql:"location_id"`
	ChecklistId        null.Int    `json:"checklist_id" sql:"checklist_id"`
	StatusId           int64       `json:"status_id" sql:"car_status" gorm:"column:car_status"`
	LocationName       null.String `json:"location_name" sql:"location_name"`
}
