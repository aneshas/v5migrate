package v5migrate

import (
	"time"

	"cloud.google.com/go/datastore"
)

type CarCheck struct {
	AppointmentID   *datastore.Key `json:"appointment_id"`
	Checklist       Checklist
	QuestionItemIDs []*datastore.Key `json:"question_item_ids"`
	QuestionItems   []QuestionItem   `datastore:"-"`
	CreatedOn       time.Time        `json:"created_on"`
	UpdatedOn       time.Time        `json:"updated_on"`
}

type QuestionItem struct {
	GroupName        string       `json:"group_name"`
	GroupOrder       int          `json:"group_order"`
	Status           int          `json:"status"`
	Title            string       `json:"title"`
	Answers          []AnswerItem `json:"answers"`
	Completed        bool         `json:"completed"`
	MechanicNotes    string       `json:"mechanic_notes"`
	MechanicFixed    bool         `json:"mechanic_fixed"`
	CustomerApproved bool         `json:"customer_approved"`
	Images           []Image      `json:"images"`
	Price            int          `json:"price"` // fixnum(2)
	Raw              string       `json:"raw"`
}

type AnswerItem struct {
	Encoded  string `json:"encoded"`
	Raw      string `json:"raw"`
	Template string `json:"template"`
}

type Image struct {
	URL               string `json:"url"`
	Active            bool   `json:"active"`
	VisibleInPDF      bool   `json:"visible_in_pdf"`
	IsFinalCheck      bool   `json:"is_final_check"`
	InterventionIndex int    `json:"intervention_index"`
}
