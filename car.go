package v5migrate

import (
	"time"

	"cloud.google.com/go/datastore"
)

// Car entity
type Car struct {
	DMSNr             string         `json:"dms_nr"`
	Make              string         `json:"make"`
	Model             string         `json:"model"`
	RegNr             string         `json:"reg_nr"`
	VINNr             string         `json:"vin_nr"`
	Reg1Date          time.Time      `json:"reg1_date"`
	Fuel              string         `json:"fuel"`
	WarrantyDateStart time.Time      `json:"warranty_date_start"`
	WarrantyDateEnd   time.Time      `json:"warranty_date_end"`
	APKDateDMS        time.Time      `json:"apk_date_dms"`
	APKDateRDW        time.Time      `json:"apk_date_rdw"`
	CurrentKM         int            `json:"current_km"`
	NextKM            int            `json:"next_km"`
	NextDate          time.Time      `json:"next_date"`
	Fleetnr           int            `json:"fleetnr"`
	FleetNumber       string         `json:"fleet_number"`
	DealerLocationID  *datastore.Key `json:"location_id"`
	AppointmentDMSNr  string         `json:"appointment_dms_nr"`

	CreatedOn time.Time `json:"created_on"`
	UpdatedOn time.Time `json:"updated_on"`
}
