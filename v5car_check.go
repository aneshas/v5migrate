package v5migrate

import "gopkg.in/guregu/null.v2"

type CarCheckV5 struct {
	ChecklistID   int64
	QuestionItems []QuestionItemV5
}

type QuestionItemV5 struct {
	ChecklistID      int64       `sql:"checklistID"`
	ChecklistName    string      `sql:"checklist_name"`
	CategoryName     string      `sql:"category_name"`
	DisplayOrder     int64       `sql:"Disp_order"`
	LocationID       null.Int    `sql:"location_id"`
	QuestionTitle    string      `sql:"sub_category_name"`
	SubDisplayOrder  int         `sql:"Sub_Disp_Order"`
	Description      null.String `sql:"description"`
	Status           string      `sql:"status"`
	Completed        string      `sql:"completed"`
	CustomerApproved string      `sql:"customer_approved"`
	Price            float64     `sql:"price"`
	ImageName        null.String `sql:"imagedisplay"`
}
