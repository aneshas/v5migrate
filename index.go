package v5migrate

import "time"

type Index struct {
	WONumber           string    `json:"wo_number"`
	TimeCarApp         time.Time `json:"date"`
	Interventions      string    `json:"interventions"`
	CarCheckStarted    bool      `json:"car_check_started"`
	IsLocal            bool      `json:"is_local"`
	IsLegacy           bool      `json:"is_legacy"`
	IsMoney            bool      `json:"is_money"`
	IsStar             bool      `json:"is_star"`
	IsShop             bool      `json:"is_shop"`
	StatusIdentifier   int       `json:"status_identifier"`
	CreatedOn          time.Time `json:"created_on"`
	AppointmentID      int64     `json:"appointment_id"`
	DealerID           int64     `json:"dealer_id"`
	DealerLocationID   int64     `json:"dealer_location_id"`
	DealerLocationName string    `json:"dealer_location_name"`

	Model            string `json:"model"`
	Make             string `json:"make"`
	VINNumber        string `json:"vin_number"`
	RegNumber        string `json:"reg_number"`
	RegNumberEscaped string `json:"reg_number_escaped"`

	CustomerName    string `json:"customer_name"`
	CustomerSurname string `json:"customer_surname"`
	CustomerEmail   string `json:"customer_email"`
}
