package v5migrate

import (
	"time"

	"cloud.google.com/go/datastore"
)

// Appointment entity
// Ancestors:
// - Car
// Decendats:
// - Intervention
// TODO - Add default values
type Appointment struct {
	DMSNr           string    `json:"dms_nr"`
	VINNr           string    `json:"vin_nr"`
	AppNr           string    `json:"app_nr"`
	WONr            string    `json:"wo_nr"`
	CarDMSNr        string    `json:"car_dms_nr"`
	DriverDMSNr     string    `json:"driver_dms_nr"`
	OwnerDMSNr      string    `json:"owner_dms_nr"`
	ContractDMSNr   string    `json:"contract_dms_nr"`
	TimeCarApp      time.Time `json:"time_car_app"`
	TimeCarIn       time.Time `json:"time_car_in"`
	TimeCarReady    time.Time `json:"time_car_ready"`
	CustomerWaiting bool      `json:"customer_waiting"`
	AFSIntervent    string    `json:"afs_intervent"`
	CarCheckStarted bool      `json:"car_check_started"`
	ImporterVersion string    `json:"importer_version"`
	IsLocal         bool      `json:"is_local"`
	IsLegacy        bool      `json:"is_legacy"`
	IsMoney         bool      `json:"is_money"`
	IsStar          bool      `json:"is_star"`
	IsShop          bool      `json:"is_shop"`

	LocationIDV5        int64  `datastore:"-"`
	LocationNameV5      string `datastore:"-"`
	AppointmentStatusV5 int64  `datastore:"-"`
	IDV5                int64  `datastore:"-"`

	CarModel  string    `json:"car_model"`
	CarMake   string    `json:"car_make"`
	RegNumber string    `json:"reg_no"`
	CurrentKM int       `json:"current_km"`
	NextKM    int       `json:"next_km"`
	NextDate  time.Time `json:"next_date"`

	AppointmentStatusID *datastore.Key `json:"appointment_status_id,omitempty"`

	CarID     *datastore.Key `json:"car_id"`
	CreatedOn time.Time      `json:"created_on"`
	UpdatedOn time.Time      `json:"updated_on"`

	Intervention     []Intervention `json:"interventions,omitempty"`
	DealerLocationID *datastore.Key `json:"dealer_location_id"`
}
