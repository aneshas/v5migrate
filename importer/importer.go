package importer

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"cloud.google.com/go/datastore"
	"golang.org/x/net/context"
	//	dstore "google.golang.org/appengine/datastore"

	"bitbucket.org/claireit/v5migrate"
)

var ErrCarNotSaved = errors.New("Car could not be saved")
var ErrAppNotSaved = errors.New("App could not be saved")
var ErrDecodeKey = errors.New("Could not decode key")

type Importer interface {
	Import(context.Context, []v5migrate.ExportEntry) (*Result, error)
}

var NoIndex = false
var Index = "dev"

type Result struct {
	EntryCount        int
	AppointmentsAdded int
	CarsAdded         int
	CarsUpdated       int
	ChecksAdded       int
	ChecksCount       int
	ErroredOut        []int64
}

func (r Result) CarsTotal() int {
	return r.CarsAdded + r.CarsUpdated
}

type datastoreImporter struct {
	client *datastore.Client
}

func NewDatastoreImporter(client *datastore.Client) (Importer, error) {
	return &datastoreImporter{client}, nil
}

func (imp *datastoreImporter) Import(c context.Context, data []v5migrate.ExportEntry) (*Result, error) {
	result := Result{
		EntryCount: len(data),
	}

	log.Printf("Starting import of %d entries", len(data))

	for _, entry := range data {
		// TODO - Implement retry
		key, err := imp.save(c, &entry, &result)
		if err != nil {
			log.Println(err)
			result.ErroredOut = append(result.ErroredOut, entry.Appointment.IDV5)
			continue
		}

		err = imp.saveChecks(c, &entry, key, &result)
		if err != nil {
			// TODO - Retry ???
		}
	}

	return &result, nil
}

func (imp *datastoreImporter) save(c context.Context, entry *v5migrate.ExportEntry, result *Result) (*datastore.Key, error) {
	carExists := false // If set to false down the line, car should not be updated
	carKey := datastore.IncompleteKey("Car", nil)

	result.ChecksCount += len(entry.Checks)

	q := datastore.NewQuery("Car").
		//Filter("DealerLocationID =", entry.Appointment.DealerLocationID).
		//Filter("DMSNr =", entry.Car.DMSNr). // Problem - we have no car dmsnr in v5
		Filter("VINNr =", entry.Car.VINNr).
		KeysOnly()

	keys, err := imp.client.GetAll(c, q, nil)
	if err == nil && len(keys) > 0 {
		carKey = keys[0]
		carExists = true
		result.CarsUpdated += 1
	}

	if !carExists {
		carKey, err = imp.client.Put(c, carKey, &entry.Car)
		if err != nil {
			return nil, ErrCarNotSaved
		}
		result.CarsAdded += 1
	}

	entry.Appointment.CarID = carKey

	// TODO - At least appointment and car check should be run in transaction
	// Car is skiped on update any way

	appKey := datastore.IncompleteKey("Appointment", nil)
	appKeyA, err := imp.client.Put(c, appKey, &entry.Appointment)
	if err != nil {
		log.Println(err)
		return nil, ErrAppNotSaved
	}

	result.AppointmentsAdded += 1

	if !NoIndex {
		setIndex(c, entry, appKeyA)
	}

	return appKeyA, err
}

func (imp *datastoreImporter) saveChecks(c context.Context, entry *v5migrate.ExportEntry, appKeyA *datastore.Key, result *Result) error {
	for _, check := range entry.Checks {
		check.AppointmentID = appKeyA

		keys := make([]*datastore.Key, len(check.QuestionItems))
		for i, _ := range keys {
			keys[i] = datastore.IncompleteKey("QuestionItem", nil)
		}

		keysA, err := imp.client.PutMulti(c, keys, check.QuestionItems)
		if err != nil {
			return fmt.Errorf("Could not save question items for %d", entry.Appointment.IDV5)
		}

		check.QuestionItemIDs = keysA

		key := datastore.IncompleteKey("CarCheck", nil)

		_, err = imp.client.Put(c, key, &check)
		if err != nil {
			return fmt.Errorf("Could not save car check for %d", entry.Appointment.IDV5)
		}

		result.ChecksAdded += 1
	}

	return nil
}

func setIndex(c context.Context, e *v5migrate.ExportEntry, appKey *datastore.Key) {
	iEntry := v5migrate.Index{
		WONumber:           e.Appointment.WONr,
		TimeCarApp:         e.Appointment.TimeCarApp,
		CarCheckStarted:    e.Appointment.CarCheckStarted,
		CreatedOn:          e.Appointment.CreatedOn,
		AppointmentID:      appKey.ID,
		DealerID:           e.Appointment.DealerLocationID.Parent.ID,
		DealerLocationID:   e.Appointment.DealerLocationID.ID,
		DealerLocationName: e.Appointment.LocationNameV5,
		StatusIdentifier:   int(e.Appointment.AppointmentStatusV5),
		Model:              e.Car.Model,
		Make:               e.Car.Make,
		VINNumber:          e.Car.VINNr,
		RegNumber:          e.Car.RegNr,
		RegNumberEscaped:   escapeRegNumber(e.Car.RegNr),
	}

	/*
		dAppKey := dstore.NewKey(c, "Appointment", "", appKey.ID, nil)
		iEntry.AppointmentID = dAppKey

		dDKey := dstore.NewKey(c, "Dealer", "", e.Appointment.DealerLocationID.Parent.ID, nil)
		iEntry.DealerID = dDKey
	*/

	if e.Appointment.Intervention != nil {
		for _, in := range e.Appointment.Intervention {
			iEntry.Interventions += (in.Title + " ")
		}
	}

	data, err := json.Marshal(iEntry)
	if err != nil {
		log.Println("ERROR: Could not json encode index entry")
		return
	}

	id := appKey.ID
	url := fmt.Sprintf("http://localhost:9200/%s/appointment/%d", Index, id)

	req, _ := http.NewRequest("PUT", url, bytes.NewReader(data))

	client := http.Client{}

	resp, err := client.Do(req)
	defer resp.Body.Close()

	b, _ := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Println("Elastic post error", string(b))
		return
	}

	log.Printf("Index for %v saved", appKey)
	log.Println(string(b))
}

func escapeRegNumber(nr string) string {
	return strings.Replace(nr, "-", "", -1)
}
