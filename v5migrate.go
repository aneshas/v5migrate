package v5migrate

type ExportEntry struct {
	Appointment Appointment
	Car         Car
	Checks      []CarCheck
}

type DealerInfo struct {
	DealerIDv5   int64
	DealerIDv6   string
	LocationInfo map[int64]string
}
