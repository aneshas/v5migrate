package main

import (
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"bitbucket.org/claireit/v5migrate"
	"bitbucket.org/claireit/v5migrate/exporter"
	"bitbucket.org/claireit/v5migrate/importer"

	"cloud.google.com/go/datastore"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

const batchSize = 100
const retry = 3

var indexes = map[string]string{
	"claire-dev": "dev",
	"claire-pro": "prod",
}

// TODO - Add config file for location info

func main() {
	t := time.Now()
	defer func() {
		log.Printf("Total time elapsed: %v", time.Since(t))
	}()

	untilStr := strings.Split(t.String(), " ")[0]

	var project = flag.String("project", "claire-dev", "GCP Project identifier")
	var noImport = flag.Bool("no_import", false, "Set to true to skip import")
	var noIndex = flag.Bool("no_index", false, "Set to true to skip elasticsearch indexing")
	var countOnly = flag.Bool("count_only", false, "Set to true to only print count")
	var mysqlUser = flag.String("mysql_user", "root", "MySQL user")
	var mysqlPwd = flag.String("mysql_pwd", "", "MySQL password")
	var db = flag.String("db", "clairei1_live", "MySQL database")
	var untilDate = flag.String("until_date", untilStr, "Until and excluding date")

	var serviceAccount = flag.String("service_account", "claire-dev-auth", "Service account key file name")

	flag.Parse()

	importer.Index = indexes[*project]
	importer.NoIndex = *noIndex

	dealerInfo := v5migrate.DealerInfo{
		DealerIDv5: 52,
		DealerIDv6: "agxlfmNsYWlyZS1wcm9yEwsSBkRlYWxlchiAgIDAr_y5CQw",
		LocationInfo: map[int64]string{
			// 111: "agxlfmNsYWlyZS1kZXZyLgsSBkRlYWxlchiAgICQzImVCQwLEg5EZWFsZXJMb2NhdGlvbhiAgICAgICACgw",
			// 112: "agxlfmNsYWlyZS1kZXZyLgsSBkRlYWxlchiAgICQzImVCQwLEg5EZWFsZXJMb2NhdGlvbhiAgICAgLmECgw",
			// 131: "agxlfmNsYWlyZS1kZXZyLgsSBkRlYWxlchiAgICQzImVCQwLEg5EZWFsZXJMb2NhdGlvbhiAgICAgPKICgw",
			1: "agxlfmNsYWlyZS1wcm9yLgsSBkRlYWxlchiAgIDAr_y5CQwLEg5EZWFsZXJMb2NhdGlvbhiAgICAgICACgw",
		},
	}

	var erroredOut []int64
	var totalHandled int

	c := context.Background()

	opt := option.WithServiceAccountFile(*serviceAccount)

	client, err := datastore.NewClient(c, *project, opt)
	if err != nil {
		log.Fatal(err)
	}

	defer client.Close()

	connString := fmt.Sprintf("%s:%s@tcp(localhost:3306)/%s", *mysqlUser, *mysqlPwd, *db)

	expo, close, err := exporter.NewMySQLExporter(connString, client, dealerInfo)
	//expo, close, err := exporter.NewMySQLExporter("root:@tcp(localhost:3306)/betacla1_claireit", client, dealerInfo)
	if err != nil {
		log.Fatal(err)
	}

	defer close()

	ids, err := expo.GetExportIDs(*untilDate)
	if err != nil {
		log.Fatal(err)
	}

	if *countOnly {
		log.Printf("Number of entries: %d", len(ids))
		return
	}

	if *noImport {
		log.Println("noimport flag set, skipping import...")
		return
	}

	log.Printf("Starting import for total of %d entries", len(ids))

	defer func() {
		log.Printf("Finished importing %d entries for total of %d", totalHandled, len(ids))
		log.Printf("ERRORED OUT ENTRIES: %v", erroredOut)
	}()

	var numRoutines int

	//	wg := sync.WaitGroup{}

	if len(ids) <= batchSize {
		numRoutines = 1
	} else {
		numRoutines = len(ids) / batchSize
		reminder := len(ids) - (numRoutines * batchSize)

		if reminder > 0 {
			numRoutines += 1
		}
	}

	imp, err := importer.NewDatastoreImporter(client)
	if err != nil {
		log.Fatal(err)
	}

	var skippedEntries []int64

	var impex = func(ids []int64, i int) error {
		offset := i * batchSize
		to := offset + batchSize

		var s []int64

		if i == (numRoutines-1) && ids[offset:] != nil {
			s = ids[offset:]
		} else {
			s = ids[offset:to]
		}

		data, err := expo.Export(c, s)
		if err != nil {
			skippedEntries = append(skippedEntries, s...)
			return fmt.Errorf("EXPORT ERROR IDS: %v, ERROR: %v", s, err)
		}

		// return nil // TODO - Remove this for import

		result, err := imp.Import(c, data)
		if err != nil {
			skippedEntries = append(skippedEntries, s...)
			return fmt.Errorf("IMPORT ERROR IDS: %v, ERROR: %v", s, err)
		}

		if result.ErroredOut != nil || len(result.ErroredOut) > 0 {
			skippedEntries = append(skippedEntries, result.ErroredOut...)
			totalHandled += len(s) - len(result.ErroredOut)
			return fmt.Errorf("IMPORT ERROR IDS: %v, ERROR: %v", result.ErroredOut, err)
		}

		totalHandled += len(s)

		log.Printf("%+v", result)
		log.Printf("Import successfull for batch %v", s)

		return nil
	}

	//	wg.Add(numRoutines)

	for i := 0; i < numRoutines; i++ {
		//		go func(i int) {
		//defer wg.Done()

		err = impex(ids, i)
		if err != nil {
			log.Println(err)
			continue
		}
		//		}(i)
	}

	if skippedEntries != nil {
		log.Printf("RETRYING SKIPPED ENTRIES %v", skippedEntries)

		retryMap := make(map[int64]int, len(skippedEntries))
		skippedEntriesPrime := make([]int64, len(skippedEntries))
		copy(skippedEntriesPrime, skippedEntries)

		skippedEntries = nil

		for {
			log.Println("Waiting 5 seconds before retry")
			time.Sleep(time.Second * 5)

			var retryEntries []int64

			for _, entry := range skippedEntriesPrime {
				numRetries, ok := retryMap[entry]

				if ok && numRetries == -1 {
					continue
				}

				if !ok || numRetries < retry {
					retryEntries = append(retryEntries, entry)
					retryMap[entry] += 1
				} else if ok && numRetries == retry {
					erroredOut = append(erroredOut, entry)
				}
			}

			if retryEntries == nil || len(retryEntries) == 0 {
				if skippedEntries == nil {
					break
				}

				copy(skippedEntriesPrime, skippedEntries)
				continue
			}

			if len(retryEntries) <= batchSize {
				numRoutines = 1
			} else {
				numRoutines = len(retryEntries) / batchSize
				reminder := len(retryEntries) - (numRoutines * batchSize)

				if reminder > 0 {
					numRoutines += 1
				}
			}

			for i := 0; i < numRoutines; i++ {
				err = impex(retryEntries, i)
				if err != nil {
					continue
				}

			outer:
				for _, e := range retryEntries {
					for _, ep := range skippedEntries {
						if ep == e {
							continue outer
						}
					}
					retryMap[e] = -1
				}

				totalHandled += 1
			}
		}
	}

	// wg.Wait()
}
