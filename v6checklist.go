package v5migrate

type Checklist struct {
	Name         string `json:"name"`
	DisplayOrder int    `json:"display_order"`
}
