package v5migrate

// Intervention entity
// Ancestors:
// - Appointment
type Intervention struct {
	DMSNr          string `json:"dms_nr"`
	IntervenID     string `json:"interven_id"`
	IntervenFOL    string `json:"interven_fol"`
	Title          string `json:"title"`
	Description    string `json:"description"`
	Sectie         string `json:"sectie"`
	AFSTimeReserve int    `json:"afs_time_reserve"` // fixnum(2)
	PriceTotalInt  int    `json:"price_total_int"`  // fixnum(2)
	MechanicNotes  string `json:"mechanic_notes"`
	MechanicFixed  bool   `json:"mechanic_fixed"`
	Status         int    `json:"status"`
	IsLocal        bool   `json:"is_local"`
	CustomerOK     bool   `json:"customer_ok"`
}
