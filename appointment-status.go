package v5migrate

type AppointmentStatus struct {
	Identifier int    `json:"identifier"`
	Name       string `json:"name"`
	Icon       string `json:"icon"`
	Color      string `json:"color"`
	Order      int    `json:"order"`
}
